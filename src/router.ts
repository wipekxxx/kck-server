import {MpcController} from './controllers/mpc.controller';
import {Request, Response} from 'express';

export class Router {
    private _app: any;
    private _mpcController: MpcController = new MpcController();

    constructor(app: any) {
        this._app = app;
    }

    public setRoutes() {
        this._app.get('/mpc',
            (req: Request, res: Response) => this._mpcController.getAll(req, res));

        this._app.get('/mpc/:id',
            (req: Request, res: Response) => this._mpcController.getById(req, res));

        this._app.get('/mpc/pads/:id/:padChar',
            (req: Request, res: Response) => this._mpcController.getPadsById(req, res));

        this._app.post('/mpc',
            (req: Request, res: Response) => this._mpcController.saveMpc(req, res));

        this._app.post('/mpc/pads/:id',
            (req: Request, res: Response) => this._mpcController.savePads(req, res));
    }
}
