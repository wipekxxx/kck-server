import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as logger from 'morgan';
import { Router } from './router';
import { Request, Response } from 'express';
import * as errorHandler from 'errorhandler';
import * as compression from 'compression';
import * as multer from 'multer';


export class Server {
    private _app: express.Application;
    private upload = multer();

    constructor() {
        this._app = express();
        this.config();
    }

    private configHeaders(): void {
        this._app.use((req: Request, res: Response, next) => {
            res.setHeader('Access-Control-Allow-Origin', '*');
            res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
            res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,Authorization');
            res.setHeader('Access-Control-Allow-Credentials', 'true');
            res.setHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
            next();
        });
    }

    private config(): void {
        this._app.use(bodyParser.urlencoded({extended: true}));
        this._app.use(bodyParser.json());
        // this._app.use(bodyParser({limit: '50mb'}));
        this._app.use(compression());
        this._app.use(logger('dev'));
        this._app.use(errorHandler());
        this._app.use(this.upload.array());

        this.configHeaders();
        this.bindRoutes();
    }

    private bindRoutes(): void {
        new Router(this._app).setRoutes();
    }

    public start(): void {
        this._app.listen(process.env.PORT || 8080, function () {
            console.log('Server app listening!')
        });
    }
}
