import { MpcService} from '../services/mpc.service';
import { Request, Response } from 'express';

export class MpcController {
    private _service: MpcService;

    constructor() {
        this._service = new MpcService();
    }

    getAll(req: Request, res: Response) {
        res.send(this._service.getAll());
    }

    getById(req: Request, res: Response) {
        res.send(this._service.getMpcById(req.params.id));
    }

    getPadsById(req: Request, res: Response) {
        res.send(this._service.getPadsByMpcId(req.params.id, req.params.padChar));
    }

    saveMpc(req: Request, res: Response) {
        res.send(this._service.saveMpc(req.body));
    }

    savePads(req: Request, res: Response) {
        res.send(this._service.savePads(req.params.id, req.body));
    }
}
