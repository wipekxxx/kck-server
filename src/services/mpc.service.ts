import * as fs from 'fs-extra';

export class MpcService {

    MPC_KEYS: String[] = [
        '1',
        '2',
        '3',
        '4',
        '5',
        '6',
        '7',
        '8',
        '9',
        'Q',
        'W',
        'E',
        'R',
        'T',
        'Y',
        'U',
        'I',
        'O',
        'A',
        'S',
        'D',
        'F',
        'G',
        'H',
        'J',
        'K',
        'L',
        'Z',
        'X',
        'C',
        'V',
        'B',
        'N',
        'M',
        ',',
        '.'
    ];

    getAll(): any {
        const data = Array();
        const dirName = './src/jsons/mpc/';

        fs.readdirSync(dirName).forEach(file => {
            if (file.includes('.json')) {
                data.push(JSON.parse(fs.readFileSync('./src/jsons/mpc/' + file)));
            }
        });

        // this.readFiles('./src/jsons/mpc/', (filename, content) => {
        //     data[filename] = content;
        // }, (err) => {
        //     console.log(err);
        // });
        // return fs.readFileSync(JSONS_PATHS.get('accounts'));
        return data;
    }

    getMpcById(id): any {
        return fs.readFileSync('./src/jsons/mpc/' + id + '.json');
    }

    getPadsByMpcId(id, padChar): any {
        return fs.readFileSync('./src/jsons/mpc/pads/' + id + '-' + padChar  + '.json');
    }

    saveMpc(body: any) {
        console.log(body);
        const fileName = './src/jsons/mpc/' + body.id + '.json';
        fs.writeFile(fileName, JSON.stringify(body), (err) => {
            if (err) {
                console.error(err);
                return err;
            }
            return 'ok';
        });
    }

    savePads(id: string, body: any) {

        console.log('Pad to save');
        console.log(body);

        this.MPC_KEYS.forEach(ch => {

            if (body['pad' + ch] != null) {
                const fileName = './src/jsons/mpc/pads/' + id + '-' + ch.charCodeAt(0) + '.json';

                fs.writeFile(fileName, body['pad' + ch], (err) => {
                    if (err) {
                        console.error(err);
                        return err;
                    }
                    return 'ok';
                });
            }
        });
    }
}
